# README #

University of Maryland CMSC 430 Compiler written by Dustin Brashear, with code adapted from Thomas Gilray.

I swear on my honor that I have niether given or received any unauthorized assistance on this project/examination.

## REQUIREMENTS ##
* BoehmGC version 7.7.0
* Racket version 6.10
* Clang version 3.9

## USAGE ##
From the command line, run `racket compiler.rkt <location of source file>`. An executable will be generated with the name bin.

## PART 1 ##

This compiler is
This compiler is designed to compile a subset of Scheme into a binary executable. As such, it supports many of the same forms and primitives as Scheme. To explicitly enumerate, the following forms are all valid:

* define
    * (define id val)
    * (define (id args ... [default-id default-val] ...) body ...+)
    * (define (id args ... . rest-id) body ...+)
* letrec\*
	* (letrec* ([id val-expr] ...) body ...+)
* letrec
	* (letrec ([id val-expr] ...) body ...+)
* let\*
	* (let* ([id val-expr] ...) body ...+)
* let
    * (let ([id val-expr] ...) body ...+)
* named let
    * (let proc-id ([id init-expr] ...) body ...+)
* lambda
    * (lambda (arg-id ...) body ...+)
    * (lambda rest-id body ...+)
    * (lambda (arg-id ... . rest-id) body ...+) 
* dynamic-wind
    * (dynamic-wind pre-thunk value-thunk post-thunk)
* guard
    * (guard id (cond-clause ...) body ...+)
    * cond-clause is either
        * (expr)
        * (test-expr then-body ...)
        * (else expr ...+)
* raise
    * (raise error-val)
* delay
    * (delay val-expr)
* force
    * (force val)
* and
    * (and val-expr ...)
* or
    * (or val-expr ...)
* match
    * (match val-expr match-clause ...)
    * match-clause is either
        * [pat body ...+]
        * [else body ...+]
        * pat is one of the following
            * nat 
            * string 
            * \#t 
            * \#f 
            * (quote dat) 
            * x 
            * (? e pat) 
            * (cons pat pat) 
            * (quasiquote qqpat)
            * qqpat is one of the following
                * e 
                * dat 
                * (unquote qqpat) 
                * (unquote pat) 
                * (quasiquote qq)
                * (qq ...+) 
                * (qq ...+ . qq)
* cond
    * (cond cond-clause)
    * cond-clause is either
        * (expr)
        * (test-expr then-body ...)
        * (else expr ...+)
* case
    * (case val-expr case-clause ...)
    * case clause is either
        * [(dat ...) body ...+]
        * [else body ...+]
* if
    * (if test-expr expr-true expr-false)
* when
    * (when test-expr body-true ...+)
* unless
    * (unless test-expr body-false ...+)
* set!
    * (set! id val-expr)
* begin
    * (begin body ...+)
* call/cc
    * (call/cc expr)
* apply
    * (apply expr arg-list)
    
The following primitive functions are also supported:

* = 
* \> 
* < 
* <= 
* \>= 
* \+ 
* \- 
* \* 
* / 
* cons?
* null?
* cons
* car
* cdr
* list
* first
* second
* third
* fourth
* fifth
* list
* length
* list\-tail
* drop
* take
* member
* memv
* map
* append
* foldl
* foldr
* vector?
* vector
* make\-vector
* vector\-ref
* vector\-set\!
* vector\-length
* set
* set\-\>list
* list\-\>set
* set\-add
* set\-union
* set\-count
* set\-rest
* set\-remove
* hash
* hash\-ref
* hash\-set
* hash\-count
* hash\-keys
* hash\-has\-key?
* hash?
* list?
* void?
* promise?
* procedure?
* number?
* integer?
* error
* void
* print
* display
* write
* exit
* halt
* eq?
* eqv?
* equal?
* not

## PART 2 ##

Unless otherwise stated, all tests for runtime errors are located in tests/error

#### Function provided too few \ too many arguments ####

To implement both of these runtime errors, I wrapped every lambda in a variadic lambda so that they became 
```
(lambda lst (if (check number of arguments here ...) (apply original-lambda lst) (raise error)
```
The check within the if statement changes depending on the type of lambda. For lambdas with a set number of arguments, I just check to see if the lst is the same length as the number of arguments, which is counted at compile time. For lambdas of the form (lambda xs ... . rest) ...), I instead check if the input list is at least as large as there are xs. Finally for default arguments, I check for at least the number of non-default args and no more than the number of default and non-default args. This error is tested with tests test-few-1, test-few-2, test-many-1, and test-many-2.

#### Non-function value is applied ####
To implement this error, a check is added at the llvm level to see if a closure is actually a closure by ...

#### Use of not-yet-initialized letrec or letrec\* variable ####
To implement this error, I add a check during the desugar step to when these forms are desugared to initialize each variable to a magic value, such as \'leterec-var-not-init, and then wrap each access of any of the variables in a check that checks the value, and if it isn't the magic value, returns normally, else it throws an error. Tested by test-init-1 and test-init-2.

#### Division by zero ####
Probably the simplest error to implement, a check is added at the llvm level to the divide prim that checks if the second argument is zero. Tested by test-zero-1 and test-zero-2.

## PART 3 ##
I decided to implement strings by creating a list of characters and adding a string tag to the front of the list. This simplifies string\-\>list into a single call to car, 